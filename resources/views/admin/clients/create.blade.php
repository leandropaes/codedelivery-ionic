@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Novo Cliente</h3>

                @include('errors._check')

                {!! Form::open(['route' => 'admin.clients.store']) !!}

                @include('admin.clients._form')

                <div class="form-group">
                    {!! Form::submit('Criar Cliente', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection