@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Novo Produto</h3>

                @include('errors._check')

                {!! Form::open(['route' => 'admin.products.store']) !!}

                @include('admin.products._form')

                <div class="form-group">
                    {!! Form::submit('Criar Produto', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection