@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Novo Cupon</h3>

                @include('errors._check')

                {!! Form::open(['route' => 'admin.cupoms.store']) !!}

                @include('admin.cupoms._form')

                <div class="form-group">
                    {!! Form::submit('Criar Cupon', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection