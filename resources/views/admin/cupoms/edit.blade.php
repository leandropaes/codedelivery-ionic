@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Editando Cupon: {{ $cupom->code }}</h3>

                @include('errors._check')

                {!! Form::model($cupom, ['route' => ['admin.cupoms.update', $cupom->id]]) !!}

                @include('admin.cupoms._form')

                <div class="form-group">
                    {!! Form::submit('Salvar Cupon', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection