@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Nova Categoria</h3>

                @include('errors._check')

                {!! Form::open(['route' => 'admin.categories.store']) !!}

                @include('admin.categories._form')

                <div class="form-group">
                    {!! Form::submit('Criar Categoria', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection