<?php

use CodeDelivery\Models\Order;
use CodeDelivery\Models\OrderItem;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order::class, 10)->create()->each(function($o) {
            $o->items()->saveMany(factory(OrderItem::class)->times(2)->make([
                'product_id' => rand(1, 10),
                'qtd' => 2,
                'price' => rand(20, 60)
            ]));
        });
    }
}
